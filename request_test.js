const webHDFS = require('webhdfs')
const request = require('request')

let hdfs = webHDFS.createClient({
  host: 'localhost',
  port: 50070,
  path: '/webhdfs/v1/'
});
let url = 'localhost'
let port = 50070;
let dirPath = 'user';
let path = '/webhdfs/v1/'+dirPath+'?op=LISTSTATUS&user.name=hduser';

let fullPath = url+':'+port+path;
let readPath = 'http://cloud-tech-dataproc-cluster-9321-m/webhdfs/v1/user?op=LISTSTATUS'
let openPath = 'http://localhost:50070/webhdfs/v1/user/hduser/test_2.txt?op=OPEN'

// list operation
/*
request(openPath, (err, res, body) => {
  console.log(fullPath2);
  if (res.statusCode == 200) {
    console.log('status code', res.statusCode);
    console.log('response body: ', body);

    //let jsonStr = JSON.parse(res);
    //let myObj = jsonStr.FileStatuses.FileStatus;
    console.log('res: ', res);
    //console.log('my obj: ', myObj);
    let objLength = Object.entries(myObj).length;
    console.log('-- number of files in the folder: ', objLength);
  
  } else {
    console.log('error occured', err);
    console.log('status code', res.statusCode);
  }
});
*/

// read the remote google hdfs
request(readPath, (err, res, body) => {
  console.log(readPath);
  console.log(res);
  if (res.statusCode == 200) {
    console.log('status code', res.statusCode);
    console.log('response body: ', body);

    let jsonStr = JSON.parse(res);
    let myObj = jsonStr.FileStatuses.FileStatus;
    let objLength = Object.entries(myObj).length;
    console.log('number of files in the folder: ', objLength);
  
  } else {
    console.log('error occured', err);
    console.log('status code', res.statusCode);
  }
});

/*
let remoteFileStream = hdfs.createReadStream('user/hduser/test.txt');

remoteFileStream.on('error', function onError (err) {
  console.log('onError(): ', err);
});

remoteFileStream.on('data', function onChunk (chunk) {
  console.log('onChunk(): ', chunk);
});


remoteFileStream.on('finish', function onFinish () {
  console.log('onFinish()');
});
*/
