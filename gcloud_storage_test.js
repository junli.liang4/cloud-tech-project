require('dotenv').config();

async function listFiles(bucketName) {
  const {Storage} = require('@google-cloud/storage');
  const storage = new Storage({
    projectId: process.env.GOOGLE_CLOUD_PROJECT,
    keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS
  });
  const [files] = await storage.bucket(bucketName).getFiles();

  console.log('Files:');
  files.forEach(file => {
    console.log(file.name);
  });
}

const bucketName = process.env.BUCKET_NAME;
console.log('bucketName: ', bucketName);
console.log('keyname: ', process.env.GOOGLE_APPLICATION_CREDENTIALS);
listFiles(bucketName);

const {Storage} = require('@google-cloud/storage');
let gcs = new Storage({
    projectId: process.env.GOOGLE_CLOUD_PROJECT,
    keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS
});

let bucket = gcs.bucket(process.env.BUCKET_NAME);
let filename = 'flights1.csv'
/* code not working 
let [files] =  gcs.bucket(process.env.BUCKET_NAME).getFiles();
console.log('Files:');
files.forEach(file => {
  console.log(file.name);
});
*/
// download the file
bucket
  .file(filename)
  .download({
    destination: `./${filename}`
  }, (err) => { 
    if (err) {
      console.log('download err: ', err);
    } else {
      console.log('download finished');
    }
  });
  /*
  .then(() => {
    console.log('download finished');
  });
  */
