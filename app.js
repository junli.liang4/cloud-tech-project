 'use strict';

 // requires
 const express = require('express');
 const exphbs = require('express-handlebars');
 const bodyParser = require('body-parser');
 const {
   Storage
 } = require('@google-cloud/storage');
 const fs = require('fs');
 require('dotenv').config();
 const csv = require('fast-csv');
 const jsregression = require('js-regression');
 const g = require('./g.js')
 const parse = require('csv-parse');

 const app = express();

 // middleware
 app.engine('handlebars', exphbs({
   defaultLayout: 'main'
 }));
 app.set('view engine', 'handlebars');
 app.use(bodyParser.urlencoded({
   extended: true
 }));

 // variables
 let spinner = false;
 let csvData = [];
 let result = undefined;
 let message = undefined;
 let predict = false;
 let regression = new jsregression.LinearRegression({
   alpha: 0.0000001,
   iterations: 100,
   lambda: 0.1
 });

 // data preload and fit
 function preLoad() {
   fs.createReadStream(process.env.FILENAME)
     .pipe(parse({
       delimiter: ','
     }))
     .on('data', function(csvrow) {
       // convert string into int
       let row = [];
       csvrow.forEach((x) => {
         row.push(+x);
       })
       // console.log(row);
       csvData.push(row);
     })
     .on('end', function() {
       //do something wiht csvData
       const data = csvData;
       console.log('read finished');
       let model = regression.fit(data);
       console.log('model: ', model);
     });
 }

// pre operation
preLoad();

 // RESTful api handling
 // -- GET
 app.get('/', (req, res) => {
   console.log('get(): /');
   res.render('index');
 });

 app.get('/form', (req, res) => {
   res.status(200)
     .send('form')
     .end();
 });

 app.get('/predict', (req, res) => {
   console.log('get(): /predict');
   res.render('form');
 });

 app.get('/test', (req, res) => {
   console.log('get() /test');
   res.status(200)
     .send('Hello, world!\n')
     .end();
 });

 // -- POST
 app.post('/predict', (req, res) => {
   // turn on the spinner
   spinner = true;

   console.log('response body: ', req.body);
   let date = req.body.predict_date;
   let airlines = req.body.airlines;
   let holiday = req.body.holiday_check;

   // date stirng split
   let dateSplit = date.split('-');
   let year = dateSplit[0];
   let month = dateSplit[1];
   let day = dateSplit[2];
   if (day.startsWith('0')) {
     day = day.split('0')[1];
   }
   console.log('year: ', year, 'month: ', month, 'day: ', day);

   // holiday validate
   if (typeof holiday == 'undefined') {
     holiday = 0;
   } else {
     holiday = 1;
   }

   console.log({
     'date': date,
     'airlines': airlines,
     'holiday': holiday,
   });

   /* uncomment the code to read data
   // download the csv file from cloud storage
   let storage = new Storage({
     projectId: process.env.GOOGLE_CLOUD_PROJECT,
     keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS
   });
   // bucket object
   let bucket = storage.bucket(process.env.BUCKET_NAME);
   // download
   bucket
     .file(process.env.FILENAME)
     .download({
       destination: process.env.DOWNLOAD_FILENAME
     }, (err) => {
       if (err) {
         console.log('file download err: ', err);
       }
     });
   */

   // embedded code - model has been defined separatly
   // in the beginning of the script.
   let inputIndex = csvData.findIndex(x => x[2] == day);
   let input = csvData[inputIndex];
   console.log('intput: ', csvData[inputIndex]);

   // predict
   let predicted_y = Math.round(regression.transform(input));
   console.log('predicted_y: ', predicted_y);
   result = predicted_y <= 0 ? 1 : undefined;
   message = result == 1 ? undefined : "not";
   predict = true;

   res.render('form', {
     predict: predict,
     result: result,
     message: message
   });

   // reset
   result = undefined;
   message = undefined;
   predict = false;
 });

 // Start the server
 const PORT = process.env.PORT || 5003;
 app.listen(PORT, () => {
   console.log(`App listening on port ${PORT}`);
   console.log('Press Ctrl+C to quit.');
 });
